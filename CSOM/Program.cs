﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.SharePoint.Client;

namespace CSOM
{
    class Program
    {
        static void Main(string[] args)
        {
            ClientContext context = new ClientContext("https://dikf365.sharepoint.com/sites/dev/");

            SecureString password = new SecureString();

            foreach (char c in "Babkaloj1".ToCharArray()) password.AppendChar(c);

            context.Credentials = new SharePointOnlineCredentials("mprzenzak@dikf365.onmicrosoft.com", password);

            //Dodawanie na liste

            List list = context.Web.Lists.GetByTitle("Wojewodztwa");
            ListItemCreationInformation newItem = new ListItemCreationInformation();
            Microsoft.SharePoint.Client.ListItem listItem = list.AddItem(newItem);
            listItem["Title"] = "nowe wojewodztwo10";
            listItem.Update();


            //Tworzenie kolejnej listy

            ListCreationInformation listInfo = new ListCreationInformation();
            listInfo.Title = "Nowa lista z CSOMa10";
            listInfo.Description = "Opis listyyyyyy";
            listInfo.TemplateType = (int)ListTemplateType.GenericList;
            List newList = context.Web.Lists.Add(listInfo);
            context.ExecuteQuery();

            //Nowy element

            ListItemCreationInformation newItemOnNewList = new ListItemCreationInformation();
            Microsoft.SharePoint.Client.ListItem listItemOnNewList = newList.AddItem(newItemOnNewList);
            listItemOnNewList["Title"] = "testttt10";
            listItemOnNewList.Update();

            var user = context.Web.EnsureUser("jsyska@dikf365.onmicrosoft.com");
            context.Load(user);

            context.ExecuteQuery();

            Console.WriteLine(user.Email);
            Thread.Sleep(6000);
        }
    }
}
